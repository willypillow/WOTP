#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <random>

using namespace std;

const long bufSize = 10 * 1024 * 1024;
char buffer[bufSize] = { 0 };
char padBuf[bufSize] = { 0 };
char bufOut[bufSize] = { 0 };

void usage();
void encode(char*, char*, char*);
void decode(char*, char*, char*);
void pad(char*, long);
int random(int);

int main(int argc, char* argv[])
{
	if(argc!=5)
	{
		usage();
	}
	else if(argv[1]==string("encode"))
	{
		encode(argv[2], argv[3], argv[4]);
	}
	else if(argv[1]==string("decode"))
	{
		decode(argv[2], argv[3], argv[4]);
	}
	else if(argv[1]==string("pad"))
	{
		int size = atoi(argv[3]);
		long realSize = 0;
		switch(argv[4][0])
		{
			case 'G': realSize = size * 1024 * 1024 * 1024; break;
			case 'M': realSize = size * 1024 * 1024; break;
			case 'K': realSize = size * 1024; break;
			default: realSize = size;
		}
		pad(argv[2], realSize);
	}
	else
	{
		usage();
	}
	cout << "Done Calculating!" << endl;
	return 0;
}

void usage()
{
	cout << "Usage:\n"
	     << "OneTimePad encode [in-file] [out-file] [out-pad]\n"
		 << "	Xors in-file with an auto-generated-pad, ends when reaches end of in-file."
	     << "OneTimePad decode [out-filename] [in-file] [pad]\n"
		 << "	Xors in-file with pad, ends when reaches end of in-file. If the pad is smaller than in-file, it'll start from the beginning again."
	     << "OneTimePad pad [out-pad-filename] [size] [G/M/K/B]\n"
	     << endl;
}

void encode(char* inFile, char* outFile, char* genPad)
{
	ifstream in;
	ofstream out, pad;

	in.open(inFile, ios::binary);
	out.open(outFile, ios::binary);
	pad.open(genPad, ios::binary);

	if(!(in&&out&&pad))
	{
		cerr << "Failed to open file!" << endl;
		return;
	}

	long length = 0;
	in.seekg(0L, ios::end);
	length = in.tellg();
	in.seekg(0L, ios::beg);
	long counter = 0;

	while(in.peek()!=EOF)
	{
		in.read(buffer, bufSize);
		int i = 0;
		for(i = 0; i<bufSize; i++)
		{
			if(!(counter<length)) break;
			padBuf[i] = random(255);
			bufOut[i] = buffer[i] ^ padBuf[i];
			counter++;
		}
		out.write(bufOut, i);
		pad.write(padBuf, i);
		out.flush();
		pad.flush();
	}

	in.close();
	out.close();
	pad.close();
}

void decode(char* outFile, char* inFile, char* padName)
{
	ifstream in, pad;
	ofstream out;

	in.open(inFile, ios::binary);
	out.open(outFile, ios::binary);
	pad.open(padName, ios::binary);

	if(!(in&&out&&pad))
	{
		cerr << "Failed to open file!" << endl;
		return;
	}

	long length = 0;
	in.seekg(0L, ios::end);
	length = in.tellg();
	in.seekg(0L, ios::beg);
	long counter = 0;

	long padLen = 0;
	pad.seekg(0L, ios::end);
	padLen = pad.tellg();
	pad.seekg(0L, ios::beg);
	long padCount = 0;

	while(in.peek()!=EOF)
	{
		in.read(buffer, bufSize);
		pad.read(padBuf, bufSize);
		int i = 0, j = 0;
		for(i = 0; i<bufSize; i++)
		{
	    	if(!(counter<length)) break;
	    	if(!(padCount<padLen))
	    	{
	    		padCount = 0; j = 0;
	    		pad.seekg(0L, ios::beg);
	    		pad.read(padBuf, bufSize);
	    	}
			bufOut[i] = buffer[i] ^ padBuf[j];
			counter++; padCount++; j++;
		}
		out.write(bufOut, i);
		out.flush();
	}

	in.close();
	out.close();
	pad.close();
}

void pad(char* filename, long size)
{
	ofstream pad;
	pad.open(filename, ios::binary);

	if(!filename)
	{
		cerr << "Failed to open file!" << endl;
		return;
	}

	long i = 0;
	int j = 0;
	while(i<size)
	{
		for(j = 0; j<bufSize&&i<size; j++)
		{
			padBuf[j] = random(255);
			i++;
		}
		pad.write(padBuf, j);
	}
}

int random(int max)
{
	 default_random_engine generator;
	 uniform_int_distribution<int> distribution(0,max);
	 return distribution(generator);
}